import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth/auth.guard';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { PokedexPageComponent } from './pages/pokedex-page/pokedex-page.component';
import { PokemonDetailPageComponent } from './pages/pokemon-detail-page/pokemon-detail-page.component';
import { TrainerPageComponent } from './pages/trainer-page/trainer-page.component';

const routes: Routes = [
  {
    path: '',
    component: LandingPageComponent
  },
  {
    path: 'trainer',
    component: TrainerPageComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'pokedex',
    component: PokedexPageComponent,
    canActivate: [ AuthGuard ]
  },
  {
    path: 'details/:id',
    component: PokemonDetailPageComponent,
    canActivate: [ AuthGuard ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

