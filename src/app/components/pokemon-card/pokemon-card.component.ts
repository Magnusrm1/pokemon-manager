import { Component, Input, OnInit } from '@angular/core';
import { PokemonService } from 'src/app/services/pokemon/pokemon.service';
import { PokemonSimple } from 'src/models/pokemon.model';

@Component({
  selector: 'app-pokemon-card',
  templateUrl: './pokemon-card.component.html',
  styleUrls: ['./pokemon-card.component.scss']
})
export class PokemonCardComponent implements OnInit {

  @Input() pokemon: PokemonSimple;
  
  pictureUrl: string = "";
  pokemonId: string = "";

  constructor() { }

  ngOnInit(): void {
    this.pokemonId = this.pokemon.url.split('/')[6];
    this.pictureUrl = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/" + this.pokemonId + ".png"
  }

}
