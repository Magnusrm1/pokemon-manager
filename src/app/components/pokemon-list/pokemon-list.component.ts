import { Component, Input, OnInit } from '@angular/core';
import { PokemonSimple } from 'src/models/pokemon.model';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {

@Input() pokemonList: PokemonSimple[]

  constructor() { }

  ngOnInit(): void {
  }

}
