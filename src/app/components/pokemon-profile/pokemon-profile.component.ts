import { Component, Input, OnInit } from '@angular/core';
import { Pokemon, PokemonSimple } from 'src/models/pokemon.model';
import { Trainer } from 'src/models/trainer.model';

@Component({
  selector: 'app-pokemon-profile',
  templateUrl: './pokemon-profile.component.html',
  styleUrls: ['./pokemon-profile.component.scss']
})
export class PokemonProfileComponent implements OnInit {

  @Input() pokemon: Pokemon;
  pokemonSimple: PokemonSimple;
  trainer: Trainer;

  constructor() { }

  ngOnInit(): void {
    this.trainer = JSON.parse(window.localStorage.getItem("trainer"));
  }

  collectPokemon() {
    this.pokemonSimple = {
      name: this.pokemon.name,
      url: "https://pokeapi.co/api/v2/pokemon/" + this.pokemon.id.toString()
    }
    this.trainer.collection.push(this.pokemonSimple);
    window.localStorage.setItem("trainer", JSON.stringify(this.trainer));
  }
}
