import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Trainer } from 'src/models/trainer.model';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router){}

  trainer: Trainer = JSON.parse(window.localStorage.getItem("trainer"))
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (this.trainer == null) {
      this.router.navigateByUrl("/");
      return false;
    }
    else if (this.trainer.name === "") {
      this.router.navigateByUrl("/");
      return false;
    } else {
      return true;
    }
  }
  
}
