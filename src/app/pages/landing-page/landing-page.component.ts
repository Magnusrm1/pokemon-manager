import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../services/auth/auth.service';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import { Trainer } from 'src/models/trainer.model';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  trainer: Trainer = {
    name: '',
    collection: []
  };

  constructor(private authService: AuthService, private router: Router) { 

  }

  onSubmit() {
    this.authService.storeTrainerName(this.trainer.name);
    window.localStorage.setItem('trainer', JSON.stringify(this.trainer));
    this.router.navigateByUrl("/pokedex");
  }

  ngOnInit(): void {
    
    if (JSON.parse(window.localStorage.getItem("trainer")) != null) {
      this.router.navigateByUrl("/pokedex")
    }
  }


}
