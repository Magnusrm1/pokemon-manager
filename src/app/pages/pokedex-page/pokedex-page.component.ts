import { Component, OnInit } from '@angular/core';
import { PokemonSimple, PokemonListResponse } from 'src/models/pokemon.model';
import {PokemonService} from '../../services/pokemon/pokemon.service';

@Component({
  selector: 'app-pokedex-page',
  templateUrl: './pokedex-page.component.html',
  styleUrls: ['./pokedex-page.component.scss']
})

export class PokedexPageComponent implements OnInit {

  pokemon: PokemonSimple[] = [];
  offset: number = 0;
  pageLimit: number = 50;
  pokemonCount: number = 0;
  nextApiString: string = "";
  prevApiString: string = "";
  error: string = "";

  constructor(private pokemonService: PokemonService) { }

  ngOnInit(): void {

    this.pokemonService.fetchPokemonList(this.pageLimit, this.offset).subscribe((pokemonListResponse: PokemonListResponse) => {
      this.pokemon = pokemonListResponse.results;
      this.nextApiString = pokemonListResponse.next;
      this.prevApiString = pokemonListResponse.previous;
      this.pokemonCount = pokemonListResponse.count;
    }, error => {
      this.error = error.message;
    });

  }

  

}
