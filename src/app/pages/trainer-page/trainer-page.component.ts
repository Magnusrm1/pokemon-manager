import { Component, OnInit } from '@angular/core';
import { Trainer } from 'src/models/trainer.model';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.scss']
})
export class TrainerPageComponent implements OnInit {

  trainer: Trainer = {
    name: "",
    collection: []
  };

  constructor() { }

  ngOnInit(): void {
    this.trainer = JSON.parse(window.localStorage.getItem("trainer"));
  }

}
