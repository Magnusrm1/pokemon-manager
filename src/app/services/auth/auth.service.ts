import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  storeTrainerName(name: string) {
    window.localStorage.setItem('trainer', name);
  }

  getTrainerName(): string {
    let name = window.localStorage.getItem('trainer');
    return name;
  }

  trainerExists(): boolean {
    return window.localStorage.getItem('trainer') != '' && window.localStorage.getItem('trainer') != null;
  }

}
