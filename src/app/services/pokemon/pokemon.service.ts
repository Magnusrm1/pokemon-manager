import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Pokemon, PokemonListResponse } from 'src/models/pokemon.model';

@Injectable({
  providedIn: 'root'
})

export class PokemonService {

  constructor(private http: HttpClient) { }

  fetchPokemonList(pageLimit: number, offset: number): Observable<PokemonListResponse> {
    return this.http.get<PokemonListResponse>("https://pokeapi.co/api/v2/pokemon?offset=" + offset + "&limit=" + pageLimit);
  }

  fetchPokemon(id: number): Observable<Pokemon> {
    return this.http.get<Pokemon>("https://pokeapi.co/api/v2/pokemon/" + id);
  }

}
