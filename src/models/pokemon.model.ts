
export interface PokemonSimple {
    name: string,
    url: string
}

export interface PokemonListResponse {
    count: number,
    next: string,
    previous: string,
    results: PokemonSimple[]
}

export interface Pokemon {
    name: string,
    moves: Move[],
    types: pkmType[],
    stats: Stat[],
    sprites: Sprites,
    hp: number,
    attack: number,
    defence: number,
    specialAttack: number,
    specialDefence: number,
    speed: number,
    abilities: Ability[],
    height: number,
    weight: number,
    base_experience: number,
    id: number
} 

export interface Move {
    move: {
        name: string
    }
}

export interface pkmType {
    type: {
        name: string
    }
}

export interface Sprites {
    front_default: string
}

export interface Stat {
    base_stat: number,
    stat: {
        name: string
    }
}

export interface Ability {
    ability: {
        name: string
    }
}