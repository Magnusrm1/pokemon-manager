import { PokemonSimple } from './pokemon.model';

export interface Trainer {
    name: string,
    collection: PokemonSimple[]
}